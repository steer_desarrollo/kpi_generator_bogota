const express = require('express')
const { connect } = require('./js/dbConnection.js')
const csv = require('csv-parser')
const fs = require('fs')

const app = express()
const pool = connect()

if (app.get('env') === 'development') {
    var livereload = require('easy-livereload')
    app.set('views', __dirname + '/views')
    app.set('view engine', 'pug')
    app.use(
        livereload({
            app: app
        })
    )
}

app.get('/', async (req, res) => {
    let done = []
    let error = []
    const readStream = fs
        .createReadStream('data/190903_consultas_2011.csv')
        .pipe(csv())
    for await (const chunk of readStream) {
        try {
            const res = await pool.query(chunk['new_query'])
            done.push(chunk['id_query'])
        } catch (e) {
            error.push({ ...chunk, error: e.toString() })
        }
    }
    res.render('index', {
        done: done,
        error: error
    })
})
app.listen(3000, async () => {
    console.log('Example app listening on port 3000!')
})
